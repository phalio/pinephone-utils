#!/bin/bash

# title="$icon_win Toggle Display Scale"

# help
printhelp () {
    echo -e "\nUsage:\n"
    echo -e "  sxmo_scale_toggle.sh [options]\n"
    echo -e "Options:\n"
    echo -e "  No option        Toggle scale factor between 2, 1.5 and 1 (customisable)"
    echo -e "  A number         Set scale factor to that number"
    echo -e "  get              Print current scale factor\n"
    echo -e "Config file:\n"
    echo -e "  Location: ~/.config/sxmo_scale"
    echo -e "  You may add to this file a list of scale factors, one number per line, ordered from highest to lowest, that will override the default toggleable factors.\n"
}
if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "help" ]; then
    printhelp
    exit 0
fi

# custom scales for toggle list
config="$HOME/.config/sxmo_scale"
if [ -f "$config" ]; then
    readarray -t scales < "$config"
fi
if [ -z $scales ]; then
    scales=("2" "1.5" "1")
fi

# get monitor
monitor="${SXMO_MONITOR:-"DSI-1"}"

# get current scale
scale_old=$(swaymsg -t get_outputs | jq -r '.[].scale')

# if get: print current scale
if [ "$1" = "get" ]; then
    echo "Current scale factor: $scale_old"
    exit 0
fi

# set scale function
setscale () {
    swaymsg -- output "$monitor" scale "$scale_new"
    echo "Scale factor changed from $scale_old to $scale_new."
    exit 0
}
    
# if $1 scale, set scale to $1 use $1 as scale if exists, otherwise toggle between 1 1.5 and 2
if [ -n "$1" ]; then
    scale_new=$1
    setscale
fi

# get index of current scale to toggle between any customisable scale values
getindex () {
    for f in "${!scales[@]}"; do
	if ! awk "BEGIN{ exit (${scales[$f]} == ${scale_old}) }"; then
	    echo "${f}"
	    break
	fi
    done
}

# if no arguments: toggle between defined set of scales
scales_index_old=$(getindex)
scales_index_new=$(($scales_index_old+1))
scale_new=${scales[$scales_index_new]}
if [ -z "$scale_new" ] || [ -z "$scales_index_old" ]; then # use index 0 if gone past last index or if outside of predefined scales
    scale_new=${scales[0]}
fi
setscale
