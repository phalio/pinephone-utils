#!/bin/bash

# help
printhelp () {
    echo -e "\nUsage:\n"
    echo -e "  pp-brightness [options]\n"
    echo -e "Options:\n"
    echo -e "  No option        Toggle brightness between 0%, 50% and 100%"
    echo -e "  A number         Set brightness to that number in percent"
    echo -e "  up               Increase brightness by 5%"
    echo -e "  down             Decrease brightness by 5%"
    echo -e "  get              Print current brightness"
    echo -e "  notif            Print current brightness to notification\n"
}
if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "help" ]; then
    printhelp
    exit 0
fi

# get brightness
brightness_dir=/sys/class/backlight/backlight/brightness
brightness_old=$(cat $brightness_dir)

# brightness values
brightness_0=156
brightness_50=1640
brightness_100=3124
brightness_range=$(($brightness_100-$brightness_0)) # 2968

# if get: print current brightness
if [ "$1" = "get" ]; then
    brightness_old_per=$(awk -v f=$brightness_old -v d=$brightness_range -v p=$brightness_0 'BEGIN {printf "%.0f", (f-p)/d*100}')
    echo "Current brightness: $brightness_old_per%"
    sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
    exit 0
fi

# if notif: print current brightness to notification
if [ "$1" = "notif" ]; then
    brightness_old_per=$(awk -v f=$brightness_old -v d=$brightness_range -v p=$brightness_0 'BEGIN {printf "%.0f", (f-p)/d*100}')
    echo "Current brightness: $brightness_old_per%"
    # check for sxmo_notify_user.sh
    if [ -n "$(which sxmo_notify_user.sh 2> /dev/null)" ]; then
	sxmo_notify=1
    fi
    if [ -n "$sxmo_notify" ]; then
	sxmo_notify_user.sh Brightness $brightness_old_per%
    else
	notify-send -t 2000 -i weather-clear-symbolic "Brightness" "$brightness_old_per%                "
    fi
    sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
    exit 0
fi

# if number: set current brightness
if [ "$1" -eq "$1" ] 2> /dev/null; then
    brightness_new_per=$1
    brightness_new=$(awk -v f=$brightness_new_per -v d=$brightness_range -v p=$brightness_0 'BEGIN {printf "%.0f", f/100*d+p}')
    echo $brightness_new > $brightness_dir
    sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
    exit 0
fi

# if up: increase by 5% (roughly 148)
if [ "$1" = "up" ]; then
    brightness_new=$(($brightness_old+148))
    # do not increase above maximum
    if [ "$brightness_old" = "$brightness_100" ]; then
	exit 0
    # make sure exactly 100% is reached despite rounding
    elif [ "$brightness_old" -ge "2968" ]; then
	echo $brightness_100 > $brightness_dir
	exit 0
    #otherwise increase by 5% 
    else
	echo $brightness_new > $brightness_dir
	sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
	exit 0
    fi
fi

# if down: increase by 5% (roughly 148)
if [ "$1" = "down" ]; then
    brightness_new=$(($brightness_old-148))
    # do not decrease below minimum
    if [ "$brightness_old" = "$brightness_0" ]; then
	exit 0
    # make sure exactly 0% is reached despite rounding
    elif [ "$brightness_old" -le "312" ]; then
	echo $brightness_0 > $brightness_dir
	exit 0
    #otherwise decrease by 5% 
    else
	echo $brightness_new > $brightness_dir
	sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
	exit 0
    fi
fi
    
# if no argument: toggle between 0%, 50% and 100%
if [ -z "$1" ]; then
    if [ "$brightness_old" -eq "$brightness_100" ]; then
	echo $brightness_0 > $brightness_dir
	sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
	exit 0
    elif [ "$brightness_old" -lt "$brightness_50" ]; then
	echo $brightness_50 > $brightness_dir
	sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
	exit 0
    elif [ "$brightness_old" -ge "$brightness_50" ] && [ "$brightness_old" -lt "$brightness_100" ]; then
	echo $brightness_100 > $brightness_dir
	sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
	exit 0
    else
	echo "Toggle error, setting to 50%."
	echo $brightness_50 > $brightness_dir
	sxmo_brightness.sh 2> /dev/null # sxmo: show brightness bar
	exit 0
    fi
fi

echo "Error"
