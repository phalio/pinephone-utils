#!/bin/bash

# title="$icon_zzz List Sleep Inhibitors"

sxmo_hook_wakelocks.sh
inhibitors=$(cat /sys/power/wake_lock)
if [ -z "$inhibitors" ]; then
    inhibitors="None"
fi
sxmo_notify_user.sh "Sleep Inhibitors" "$inhibitors" 2>/dev/null
echo -e "\e[1mSleep Inhibitors\e[0m"
echo -e "$inhibitors"
