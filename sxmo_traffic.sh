#!/bin/bash

# help
printhelp () {
    echo -e "\nThis script calculates the average used upload and download bandwidth in bits per second since its last invocation.\n"
    echo -e "Usage:\n"
    echo -e "  sxmo_traffic.sh <interval>\n"
    echo -e "Options:\n"
    echo -e "  No option        Print traffic once, good for status bar"
    echo -e "  <interval>       Interval in seconds to update traffic, prints new lines with timestamps\n"
}
if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "help" ]; then
    printhelp
    exit 0
fi

# source sxmo icons
. sxmo_hook_icons.sh

# directory for temporary data
trafficpath=$HOME/.cache/sxmo_traffic

# main function
get_traffic () {
    # find out currently used network device
    if [ ! $(iw wlan0 info | grep ssid | tr -d 'ssid ' | sed -e 's/^[ \t]*//') = "" ]; then
	network_device=wlan0
    else
	network_device=wwan0
    fi
    # if no previous data, create and print 0b
    if [ ! -d $trafficpath ]; then
	mkdir $trafficpath
	echo $(date +%s) > $trafficpath/lastunixtimestamp
	echo $(ip -s -c link show $network_device | sed '4q;d' | awk '{print $1}') > $trafficpath/lasttotal_download
	echo $(ip -s -c link show $network_device | sed '6q;d' | awk '{print $1}') > $trafficpath/lasttotal_upload
	printf "$icon_ard 0b $icon_aru 0b"
	return 0
    fi
    # get times and calculate time to last measurement
    unixtimestamp=$(date +%s)
    lastunixtimestamp=$(cat $trafficpath/lastunixtimestamp)
    echo $(date +%s) > $trafficpath/lastunixtimestamp
    timestamp_difference=$(($unixtimestamp-$lastunixtimestamp))
    # get raw total traffic and calculate difference
    lasttotal_download=$(cat $trafficpath/lasttotal_download)
    lasttotal_upload=$(cat $trafficpath/lasttotal_upload)
    echo $(ip -s -c link show $network_device | sed '4q;d' | awk '{print $1}') > $trafficpath/lasttotal_download
    echo $(ip -s -c link show $network_device | sed '6q;d' | awk '{print $1}') > $trafficpath/lasttotal_upload
    total_download=$(ip -s -c link show $network_device | sed '4q;d' | awk '{print $1}')
    total_upload=$(ip -s -c link show $network_device | sed '6q;d' | awk '{print $1}')
    download_difference=$(($total_download-$lasttotal_download))
    upload_difference=$(($total_upload-$lasttotal_upload))
    # convert to bits instead of bytes (comment out the following two lines and remove both "_bit" in the section after this if you want bytes)
    download_difference_bit=$(($download_difference*8))
    upload_difference_bit=$(($upload_difference*8))
    # calculate traffic per second since last measurement
    download_persecond=$(($download_difference_bit/$timestamp_difference))
    upload_persecond=$(($upload_difference_bit/$timestamp_difference))
    # find and convert to appropriate unit (bit, kibibit, mebibit, gibibit, tebibit)
    if [ $download_persecond -lt 0 ]; then
	download_unit=b
	traffic_download=0
    elif [ $download_persecond -lt 1024 ]; then
	download_unit=b
	traffic_download=$download_persecond
    elif [ $download_persecond -ge 1024 ] && [ $download_persecond -lt 1048576 ]; then
	download_unit=K
	traffic_download=$(($download_persecond/1024))
    elif [ $download_persecond -ge 1048576 ] && [ $download_persecond -lt 1073741824 ]; then
	download_unit=M
	traffic_download=$(($download_persecond/1048576))
    elif [ $download_persecond -ge 1073741824 ] && [ $download_persecond -lt 1099511627776 ]; then
	download_unit=G
	traffic_download=$(($download_persecond/1073741824))
    else
	download_unit=T
	traffic_download=$(($download_persecond/1099511627776))
    fi
    if [ $upload_persecond -lt 0 ]; then
	upload_unit=b
	traffic_upload=0
    elif [ $upload_persecond -lt 1024 ]; then
	upload_unit=b
	traffic_upload=$upload_persecond
    elif [ $upload_persecond -ge 1024 ] && [ $upload_persecond -lt 1048576 ]; then
	upload_unit=K
	traffic_upload=$(($upload_persecond/1024))
    elif [ $upload_persecond -ge 1048576 ] && [ $upload_persecond -lt 1073741824 ]; then
	upload_unit=M
	traffic_upload=$(($upload_persecond/1048576))
    elif [ $upload_persecond -ge 1073741824 ] && [ $upload_persecond -lt 1099511627776 ]; then
	upload_unit=G
	traffic_upload=$(($upload_persecond/1073741824))
    else
	upload_unit=T
	traffic_upload=$(($upload_persecond/1099511627776))
    fi
    # print results
    printf "$icon_ard ${traffic_download}$download_unit $icon_aru ${traffic_upload}$upload_unit"
}

# user arguments: no argument: print once, otherwise $1 is update interval in seconds
if [ "$1" = "" ]; then
    get_traffic
else
    while true; do
	printf "$(date '+%F %T') $(get_traffic) \n"
	sleep $1
    done
fi
