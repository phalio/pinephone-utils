#!/bin/bash

# help
printhelp () {
    echo -e "\nUsage:\n"
    echo -e "  wp-output <keyword>\n"
    echo -e "Options:\n"
    echo -e "  <keyword>        Any part of the name of a desired output device\n"
    echo -e "To get a list of the names of currently available outputs (sinks), use 'wpctl status' and take a look at the Audio -> Sinks section. To switch to the PinePhone's outputs, you could use the keyword 'speaker'.\n"
    echo -e "This only switches the output device, e.g. to a bluetooth device, but not the output port for devices that include multiple ports. The PinePhone’s speaker, earpiece and headphone jack are ports of the same device. To switch between those, the PinePhone must be set as the audio device, e.g. with this script, and then the port must be set, e.g. with 'pp-audio-output' from this repository (that script also sets the device to the PP).\n"
}
if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "help" ]; then
    printhelp
    exit 0
fi

# check for wireplumber
if [ -z "$(which wpctl 2> /dev/null)" ]; then
    echo "It seems that your system is not using WirePlumber. If you still really want to use this, contact me via https://lilyb.it/links or open an issue and I'll see if I'm able to make a version for your audio system (likely Pulse)."
exit 1
fi

# make sure at least 1 agument is used
if [ "$1" = "" ]; then
    echo "Please specify a sink name."
    exit 1
fi

# get sink id
sink_id=$(wpctl status | grep -iE "$1.*vol" | awk '{print $2}' | tr -dc '0-9 *')

# check if desired sink is already default or does not exist
if [ "$sink_id" = "*" ]; then
    echo "This is already the default sink."
    exit 0
elif [ "$sink_id" = "" ]; then
    echo "No sink containing '$1' could be found."
    exit 0
fi

# set default sink
wpctl set-default $sink_id

# update sxmo status bar to show volume and icon of new sink
sxmo_hook_statusbar.sh volume 2> /dev/null
