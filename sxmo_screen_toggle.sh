#!/bin/sh

# title="$icon_lck Toggle Screen"

swaymsg output DSI-1 dpms toggle

if [ "$(cat $SXMO_STATE)" = "unlock" ]; then
    sxmo_hook_screenoff.sh &
else
    sxmo_hook_unlock.sh &
fi
