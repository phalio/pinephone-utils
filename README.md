# pinephone-utils

Small but useful miscellaneous things I use on my PinePhone, some exclusive to Sxmo, some especially useful in combination with the/a physical keyboard as global shortcuts.

Some work on all devices including desktop PCs but I initially made them for the PinePhone and have no other place for them yet. The file names should indicate where they can be used.

## Content

1. [pp-flashlight](#pp-flashlight) – Toggle flashlight
2. [pp-brightness](#pp-brightness) – Increase or decrease brightness by 5%, toggle between 0%, 50% and 100% or set it directly
3. [pp-audio-output](#pp-audio-output) – Set the PinePhone’s audio output port (speaker, earpiece, headphones)
4. [sxmo_autosuspend.sh](#sxmo_autosuspend-sh) – Toggle Sxmo auto suspend
5. [sxmo_scale.sh](#sxmo_scale-sh) – Set the Sway scale factor or toggle it between customisable values
6. [sxmo_traffic.sh](#sxmo_traffic-sh) – Get the currently used download and upload bandwidth, either for the status bar or to log it
7. [sxmo_sleepinhibitors.sh](#sxmo_sleepinhibitors-sh) – Get a notification listing what currently inhibits sleep
8. [sxmo_screen_toggle.sh](#sxmo_screen_toggle-sh) – Toggle the screen on and off
9. [sxmo_swaylock.sh](#sxmo_swaylock-sh) – Start swaylock and switch to the Sxmo unlock state after unlocking
10. [sway-random-wallpaper](#sway-random-wallpaper) – Set random wallpapers for all or specified monitors
11. [wp-volume](#wp-volume) – Set, increase, decrease or get the current volume
12. [wp-output](#wp-output) – Set the WirePlumber audio output device based on the first one that matches the given keyword
13. [bluetooth-toggle](#bluetooth-toggle) – Toggle or set Bluetooth

## pp-flashlight

**Toggle flashlight**

This simply toggles the flashlight on and off, with absolutely no additional unnecessary shenanigans. As a certified musician and gamer who can indeed tell the difference between Virtually No Latency™ and an input lag of above half a millisecond, I approve this as entirely Latency-Free™ (trademark by Bluetooth®), unlike the flashlight feature/script of Sxmo.

## pp-brightness

**Increase or decrease brightness by 5%, toggle between 0%, 50% and 100% or set it directly**

```
Usage:

  pp-brightness [options]

Options:

  No option        Toggle brightness between 0%, 50% and 100%
  A number         Set brightness to that number in percent
  up               Increase brightness by 5%
  down             Decrease brightness by 5%
  get              Print current brightness
  notif            Print current brightness to notification
```

Depending on your system it might require sudo. This is of course less optimal for keyboard shortcuts. To solve this, you can define pp-brightness to not require a password. Use `sudo visudo` (if you prefer a specific text editor, use e.g. `sudo EDITOR=nano visudo` and add the following line (replace the username and paths with yours):

```
lily ALL=NOPASSWD: /home/lily/git/pinephone-utils/pp-brightness
```

Now it still requires using `sudo` but you won’t have to provide a password anymore. Don’t forget to also include the `sudo` in the shortcut command!

## pp-audio-output

**Set the PinePhone’s audio output port (speaker, earpiece, headphones)**

This uses `pactl` and therefore works with PulseAudio and PipeWire, so basically everything. It can only set the ports mentioned above. To switch to anything else like a Bluetooth device you will have to use [wp-output](#wp-output) instead.

```
Usage:

  pp-audio-output <port>

Options:

  <port>           The name of the desired port (speaker, earpiece, headphones or just the first letter)
```

## sxmo_autosuspend.sh

**Toggle Sxmo auto suspend**

This simply toggles automatic suspension/sleep on and off, with a notification so you know what happened.

## sxmo_scale.sh

**Set the Sway scale factor or toggle it between customisable values**

```
Usage:

  sxmo_scale_toggle.sh [options]

Options:

  No option        Toggle scale factor between 2, 1.5 and 1 (customisable)
  A number         Set scale factor to that number
  get              Print current scale factor

Config file:

  Location: ~/.config/sxmo_scale
  You may add to this file a list of scale factors, one number per line, ordered from highest to lowest, that will override the default toggleable factors.
```

## sxmo_traffic.sh

**Get the currently used download and upload bandwidth, either for the status bar or to log it**

This script calculates the average used upload and download bandwidth in bits per second since its last invocation, so the first time will always display 0.

```
Usage:

  sxmo_traffic.sh <interval>

Options:

  No option        Print traffic once, good for status bar
  <interval>       Interval in seconds to update traffic, prints new lines with timestamps
```

If you want to display this in your status bar, copy the default hook to your home directory if you haven’t already by using `cp /usr/share/sxmo/default_hooks/sxmo_hook_statusbar.sh ~/.config/sxmo/hooks/`. Then edit `~/.config/sxmo/hooks/sxmo_hook_statusbar.sh` and have it use `sxmo_traffic.sh` somewhere, e.g. simply by adding the line `sxmo_traffic.sh | sxmo_status.sh add 6-traffic` into a new line of the `set_time()` function. This would update it about once per minute along with the time display. Adjust the number of `6-traffic` in this example to change its position in the status bar. You can get a list of the currently displayed elements by using `sxmo_status.sh debug` to easily see what number you should use to fit it between the desired existing entries.

## sxmo_sleepinhibitors.sh

**Get a notification listing what currently inhibits sleep**

I have this bound to a global keyboard shortcut along with one to end all SSH sessions with `killall sshd`. Gone are the days of being annoyed by the phone not going to sleep. Now if I notice it happening, I can just press a button and know what inhibits sleep, and if it’s a forgotten SSH session as it usually is, I can solve that with another quick button press.

## sxmo_screen_toggle.sh

**Toggle the screen on and off**

This toggles the screen on and off without the "lock" status in between.

## sxmo_swaylock.sh

**Start swaylock and switch to the Sxmo unlock state after unlocking**

It requires swaylock. [Swaylock-effects](https://github.com/jirutka/swaylock-effects) (`swaylock-effects-git` on the AUR) is recommended to display time and date. To incorporate this into Sxmo’s states, see [https://lilyb.it/tech/sxmo#lockscreen](https://lilyb.it/tech/sxmo#lockscreen).

## sway-random-wallpaper

**Set random wallpapers for all or specified monitors**

A config file at `~/.config/sway-random-wallpaper` with at least `wallpapers="/path/to/wallpaper/directory/"` is required.

```
Usage:

  sway-random-wallpaper [options]

Options:

  No option        Change wallpapers on all monitors
  Monitor name(s)  Change wallpapers on specified monitors separated by spaces
  get              Get monitor names
  set              Followed by monitor name and path to wallpaper file to specifically set a monitor to a not random wallpaper

Config file:

  Location: ~/.config/sway-random-wallpaper
  Possible variables:
    wallpapers="/path/to/wallpaper/directory/" (required, must end with /)
    fill_mode=fill, fit, stretch, center or tile (default is fill)
```

## wp-volume

**Set, increase, decrease or get the current volume**

```
Usage:

  wp-volume [options]

Options:

  A number         Set volume to that number in percent
  up               Increase volume by 5%
  down             Decrease volume by 5%
  get              Print current volume and create notification
```

It uses WirePlumber and is especially useful for non-Sxmo users, but also for them since it is slightly faster than Sxmo’s included volume script and a lot faster than the volume buttons with the inputhandler script.

## wp-output

**Set the WirePlumber audio output device based on the first one that matches the given keyword**

```
Usage:

  wp-output <keyword>

Options:

  <keyword>        Any part of the name of a desired output device
```

To get a list of the names of currently available outputs (sinks), use `wpctl status` and take a look at the Audio -> Sinks section. To switch to the PinePhone’s outputs, you could use the keyword "speaker". 

This only switches the output device, e.g. to a Bluetooth device, but not the output port for devices that include multiple ports. The PinePhone’s speaker, earpiece and headphone jack are ports of the same device. To switch between those, the PinePhone must be set as the audio device, e.g. with this script, and the port must be set, e.g. with [pp-audio-output](#pp-audio-output) (that script also sets the device to the PP).

## bluetooth-toggle

**Toggle or set Bluetooth**

This requires the `bluetooth` systemd service to be active. The service being active does not equal the Bluetooth adapter being powered on. By default, Bluetooth devices will be powered on after boot. To change this, edit `/etc/bluetooth/main.conf` with sudo and change `AutoEnable=true` to false. Having the systemd service on and AutoEnable off allows you to start out with the Bluetooth adapter being off for power-saving and/or privacy reasons while being able to immediately and easily toggle it on when you need it.

```
Usage:

  bluetooth-toggle [options]

Options:

  No option        Toggle the bluetooth device on and off
  on/off           Set the bluetooth device on or off
  status           Get the current status
```
