#!/bin/bash

# title="$icon_zzz Toggle Auto Suspend"

susfile="$XDG_CACHE_HOME/sxmo/sxmo.nosuspend"

if [ -f "$susfile" ]; then
    rm $susfile && sxmo_notify_user.sh "Auto Suspend" "Activated"
elif [ ! -f "$susfile" ]; then
    echo "" > $susfile && sxmo_notify_user.sh "Auto Suspend" "Deactivated"
else
    sxmo_notify_user.sh "Auto Suspend" "Error"
fi

sxmo_hook_statusbar.sh wakelock & # for custom statusbar integrations
